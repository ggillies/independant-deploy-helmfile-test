# Test repo for a helmfile setup that could be reused for independant component deploys of gitlab.com

## To Use
```
# Pass component as a state value to helmfile template, e.g.
helmfile --state-values-set component=kas template

# Another example for sidekiq
helmfile --state-values-set component=sidekiq template
```
